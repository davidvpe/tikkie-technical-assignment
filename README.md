# Welcome the Tikkie Person Microservice Assignment

## Introduction
This project is a microservice that is part of a larger system. The microservice is responsible for managing persons. The microservice is written in Typescript and uses CDK and Lambdas along with other AWS Services.

There are a couple of assumptions made in this project:

- The UserPool will be created and a custom authorizer will be created for the API Gateway.
  
- List People will only be reached from the API since the users expect a response right away. This can also be made available with a topic as the Create Person flow is and the result can be send out using AWS SES/SNS. But since there is no more information about the use case, I decided to keep it simple.

- List People will have a custom page size of 10.

- Create people will be reached from API Gateway and a topic. The API Gateway will be used for an authenticated user to create a person and the topic will be used by the other microservices to create a person.

- Person's phone number is unique. This is a business rule that is not mentioned in the assignment but I think it is a good rule to have.

- In order to use the API, an Authorization token is needed. More information on how to obtain it will be explained down below. Token should be used **without** the `Bearer` prefix.

## Architecture

The architecture is based on the following diagram:

![Architecture](./assets/diagram.png)

The `cdk.json` file tells the CDK Toolkit how to execute the app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template

## User Management

As mentioned before, in order to use the API, a valid Bearer token is needed. This token can be obtained by calling Cognito UserPool using the CLI or API to get a token. The token will be valid for 1 hour.

A User Pool and a User Pool Client will be created by CDK and output their respective IDs to be used on the following commands.

### Create User

In order to create a user, the following command can be used:

```bash
scripts/create-user.sh <USER_POOL_ID> <USERNAME> <PASSWORD>
```
### Get Token

In order to get a token, the following command can be used:

```bash
scripts/get-token.sh <USER_POOL_ID> <USER_POOL_CLIENT_ID> <USERNAME> <PASSWORD>
```

## Using the API

The API can be used by calling the following endpoints:

- `GET /people` - List all people
- `POST /people` - Create a person

### List People

In order to list all people, the following command can be used:

```bash
curl -X GET \
  'https://9m9ljmqn1l.execute-api.us-east-1.amazonaws.com/api/person' \
  --header 'Authorization: <COGNITO_ID_TOKEN>'
```

When more than 10 items are in the database, the response will contain a `nextToken` that can be used to get the next page of results.

```bash
curl -X GET \
  'https://9m9ljmqn1l.execute-api.us-east-1.amazonaws.com/api/person?nextToken=<NEXT_TOKEN>' \
  --header 'Authorization: <COGNITO_ID_TOKEN>'
```

### Create Person

In order to create a person, the following command can be used:

```bash
curl -X POST \
  'https://9m9ljmqn1l.execute-api.us-east-1.amazonaws.com/api/person' \
  --header 'Authorization: <COGNITO_ID_TOKEN>' \
  --data-raw '{
  "first_name": "David",
  "last_name": "Velarde",
  "phone": "+31612345678"",
  "address": "Oliebollen 24-C, 1234 AB, HappyCity"
}'
```