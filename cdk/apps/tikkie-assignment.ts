#!/usr/bin/env node
import 'source-map-support/register'
import * as cdk from 'aws-cdk-lib'
import { TikkieAssignmentStack } from '../stacks/tikkie-assignment-stack'

const app = new cdk.App()

new TikkieAssignmentStack(app, 'tikkie-assignment-stack')
