import { Duration } from 'aws-cdk-lib'
import {
  AuthorizationType,
  CognitoUserPoolsAuthorizer,
  LambdaIntegration,
  RestApi,
  RestApiProps,
} from 'aws-cdk-lib/aws-apigateway'
import { IUserPool } from 'aws-cdk-lib/aws-cognito'
import { IFunction } from 'aws-cdk-lib/aws-lambda'
import { Construct } from 'constructs'

type HttpMethod = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE' | 'ANY'

type CustomApiGatewayProps = RestApiProps & {
  userPool?: IUserPool
}

export class CustomApiGateway extends RestApi {
  private cognitoAuthorizer?: CognitoUserPoolsAuthorizer

  constructor(scope: Construct, id: string, props: CustomApiGatewayProps) {
    super(scope, id, {
      ...props,
      deployOptions: {
        stageName: 'api',
      },
    })

    if (props.userPool) {
      this.cognitoAuthorizer = new CognitoUserPoolsAuthorizer(this, 'person-users-pool-authorizer', {
        authorizerName: 'person-users-pool-authorizer',
        cognitoUserPools: [props.userPool],
        identitySource: 'method.request.header.Authorization',
        resultsCacheTtl: Duration.minutes(30),
      })
    }
  }

  addRoute(type: HttpMethod, path: string, handler: IFunction) {
    path = path.replace(/^\/|\/$/g, '')

    const resource = path.split('/').reduce((resource, path) => {
      const existingResource = resource.getResource(path)
      if (existingResource) return existingResource
      return resource.addResource(path)
    }, this.root)

    resource.addMethod(type, new LambdaIntegration(handler), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: this.cognitoAuthorizer,
    })
  }
}
