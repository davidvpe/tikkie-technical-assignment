import { CfnOutput } from 'aws-cdk-lib'
import { IUserPool, UserPool, UserPoolClient } from 'aws-cdk-lib/aws-cognito'
import { Construct } from 'constructs'

export class CognitoPool extends Construct {
  userPool: IUserPool

  constructor(scope: Construct, id: string) {
    super(scope, id)

    const userPool = new UserPool(this, 'person-users-pool', {
      userPoolName: 'person-user-pool',
      signInAliases: {
        username: true,
      },
      passwordPolicy: {
        minLength: 8,
      },
      standardAttributes: {
        fullname: {
          required: true,
          mutable: true,
        },
      },
    })

    const userPoolClient = new UserPoolClient(this, 'person-users-client', {
      userPoolClientName: 'person-users-pool-client',
      userPool,
      generateSecret: false,
      authFlows: {
        adminUserPassword: true,
      },
    })

    this.userPool = userPool

    new CfnOutput(this, 'user-pool-id', {
      exportName: 'person-user-pool-id',
      value: userPool.userPoolId,
    })

    new CfnOutput(this, 'user-pool-client-id', {
      exportName: 'person-user-pool-client-id',
      value: userPoolClient.userPoolClientId,
    })
  }
}
