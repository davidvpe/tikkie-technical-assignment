import { Duration } from 'aws-cdk-lib'
import { Alarm, ComparisonOperator } from 'aws-cdk-lib/aws-cloudwatch'
import { Runtime } from 'aws-cdk-lib/aws-lambda'
import { NodejsFunction, NodejsFunctionProps } from 'aws-cdk-lib/aws-lambda-nodejs'
import { RetentionDays } from 'aws-cdk-lib/aws-logs'
import { Construct } from 'constructs'

type CustomLambdaProps = NodejsFunctionProps & {}

export class TypescriptLambda extends NodejsFunction {
  constructor(scope: Construct, props: CustomLambdaProps) {
    super(scope, props.functionName ?? 'lambda-without-function-name', {
      logRetention: RetentionDays.ONE_MONTH,
      runtime: Runtime.NODEJS_16_X,
      ...props,
    })

    const metric = this.metricErrors({
      period: Duration.minutes(1),
    })

    new Alarm(scope, `${props.functionName}-error-alarm`, {
      alarmName: `${props.functionName}-error-alarm`,
      metric: metric,
      threshold: 1,
      comparisonOperator: ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      evaluationPeriods: 1,
      alarmDescription: props.description,
    })
  }
}
