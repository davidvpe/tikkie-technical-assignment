import { Alarm, ComparisonOperator } from 'aws-cdk-lib/aws-cloudwatch'
import { Queue, QueueProps } from 'aws-cdk-lib/aws-sqs'
import { Construct } from 'constructs'

export class QueueWithAlarm extends Queue {
  constructor(scope: Construct, id: string, props: QueueProps) {
    super(scope, id, props)
    const metric = this.metricApproximateNumberOfMessagesVisible()
    new Alarm(scope, `${id}-alarm`, {
      metric: metric,
      threshold: 1,
      comparisonOperator: ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      evaluationPeriods: 1,
    })
  }
}
