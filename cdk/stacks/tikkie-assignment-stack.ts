import * as cdk from 'aws-cdk-lib'
import { Construct } from 'constructs'
import { CognitoPool } from '../constructs/cognito'
import { CustomApiGateway } from '../constructs/api-gateway'
import { ITopic, Topic } from 'aws-cdk-lib/aws-sns'
import { Queue } from 'aws-cdk-lib/aws-sqs'
import { SqsSubscription } from 'aws-cdk-lib/aws-sns-subscriptions'
import { Effect, PolicyStatement } from 'aws-cdk-lib/aws-iam'
import { SqsEventSource } from 'aws-cdk-lib/aws-lambda-event-sources'
import { TypescriptLambda } from '../constructs/lambda'
import { AttributeType, ITable, Table } from 'aws-cdk-lib/aws-dynamodb'
import { QueueWithAlarm } from '../constructs/queue-alarm'

export class TikkieAssignmentStack extends cdk.Stack {
  private personTable: ITable
  private personCreatedTopic: ITopic

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)

    const cognito = new CognitoPool(this, 'person-authentication')

    const apiGateway = new CustomApiGateway(this, 'api-gateway', {
      userPool: cognito.userPool,
    })

    const createPersonTopic = new Topic(this, 'create-person-topic', {
      topicName: 'create-person-topic',
    })

    const personCreatedTopic = new Topic(this, 'person-created-topic', {
      topicName: 'person-created-topic',
    })

    this.personCreatedTopic = personCreatedTopic

    const newPersonDLQ = new QueueWithAlarm(this, 'new-person-dlq', {
      queueName: 'new-person-dlq',
    })

    const newPersonQueue = new Queue(this, 'new-person-queue', {
      queueName: 'new-person-queue',
      deadLetterQueue: {
        maxReceiveCount: 3,
        queue: newPersonDLQ,
      },
    })

    createPersonTopic.addSubscription(new SqsSubscription(newPersonQueue))

    const personTable = new Table(this, 'people-table', {
      tableName: 'people-table',
      partitionKey: {
        name: 'id',
        type: AttributeType.STRING,
      },
    })

    this.personTable = personTable

    const listPeopleLambda = this.listPeopleLambdaGenerator()
    apiGateway.addRoute('GET', '/person', listPeopleLambda)

    const createPersonProxyLambda = this.createPersonProxyLambdaGenerator()

    apiGateway.addRoute('POST', '/person', createPersonProxyLambda)

    const createPersonLambda = this.createPersonLambdaGenerator()

    const createPersonEventSource = new SqsEventSource(newPersonQueue, {
      reportBatchItemFailures: true,
    })

    createPersonLambda.addEventSource(createPersonEventSource)
  }

  private listPeopleLambdaGenerator() {
    const lambda = new TypescriptLambda(this, {
      functionName: 'list-people-lambda',
      entry: 'src/lambdas/list-people-lambda.ts',
      environment: {
        PERSON_TABLE_NAME: this.personTable.tableName,
      },
    })

    // Permissions to read from the table
    lambda.addToRolePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['dynamodb:Scan'],
        resources: [this.personTable.tableArn],
      }),
    )

    return lambda
  }

  private createPersonProxyLambdaGenerator() {
    const lambda = new TypescriptLambda(this, {
      functionName: 'create-person-proxy-lambda',
      entry: 'src/lambdas/create-person-proxy-lambda.ts',
      environment: {
        PERSON_TABLE_NAME: this.personTable.tableName,
        PERSON_CREATED_TOPIC_ARN: this.personCreatedTopic.topicArn,
      },
    })

    // Permissions to send messages to the topic
    lambda.addToRolePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['dynamodb:PutItem', 'dynamodb:GetItem'],
        resources: [this.personTable.tableArn],
      }),
    )

    // Permissions to send messages to the topic
    lambda.addToRolePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['sns:Publish'],
        resources: [this.personCreatedTopic.topicArn],
      }),
    )

    return lambda
  }

  private createPersonLambdaGenerator() {
    const lambda = new TypescriptLambda(this, {
      functionName: 'create-person-lambda',
      entry: 'src/lambdas/create-person-lambda.ts',
      environment: {
        PERSON_TABLE_NAME: this.personTable.tableName,
        PERSON_CREATED_TOPIC_ARN: this.personCreatedTopic.topicArn,
      },
    })

    // Permissions to write to the table
    lambda.addToRolePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['dynamodb:PutItem', 'dynamodb:GetItem'],
        resources: [this.personTable.tableArn],
      }),
    )

    // Permissions to send messages to the topic
    lambda.addToRolePolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['sns:Publish'],
        resources: [this.personCreatedTopic.topicArn],
      }),
    )

    return lambda
  }
}
