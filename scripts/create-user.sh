#! /bin/bash

# This script creates a new user on cognito and then resets its password

# The script takes 4 arguments:
# 1. The user pool id
# 2. The username
# 3. The user password

# The script echoes success or failure and the aws cli to be installed and configured

aws cognito-idp admin-create-user --user-pool-id $1 --username $2

aws cognito-idp admin-set-user-password --user-pool-id $1 --username $2 --password $3 --permanent

