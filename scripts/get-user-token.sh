#! /bin/bash

# This script gets a user token from cognito

# The script takes 3 arguments:
# 1. The user pool id
# 2. The user pool client id
# 3. The username
# 4. The user password

# The script echoes the token if succeeds and error if fails

export ID_TOKEN=$(aws cognito-idp admin-initiate-auth --user-pool-id $1 --client-id $2 --auth-flow ADMIN_NO_SRP_AUTH --auth-parameters USERNAME=$3,PASSWORD=$4 | jq -r '.AuthenticationResult.IdToken')

echo $ID_TOKEN



