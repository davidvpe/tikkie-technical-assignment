import { Person, validatePersonRequestStructure, validatePersonStructure } from '../models/person'

import {
  DynamoDBClient,
  PutItemCommand,
  PutItemCommandInput,
  GetItemCommand,
  GetItemCommandInput,
  ScanCommand,
  ScanCommandInput,
  AttributeValue,
} from '@aws-sdk/client-dynamodb'

import { SNSClient, PublishCommand, PublishCommandInput } from '@aws-sdk/client-sns'

import { CreatePersonResponse, ListPeopleResponse } from '../models/api'

class PersonControllerBase {
  constructor() {}

  protected parseAndCheckPerson = (json: Record<string, AttributeValue>): Person | undefined => {
    const possiblePerson: Partial<Person> = {
      id: json.id.S,
      first_name: json.first_name.S,
      last_name: json.last_name.S,
      phone: json.phone.S,
      address: json.address.S,
    }

    return validatePersonStructure(possiblePerson)
  }
}

class PersonReadController extends PersonControllerBase {
  constructor(protected dynamoDbClient: DynamoDBClient) {
    super()
  }

  listPeople = async (nextToken?: string): Promise<ListPeopleResponse> => {
    const params: ScanCommandInput = {
      TableName: process.env.PERSON_TABLE_NAME,
      ExclusiveStartKey: nextToken ? { id: { S: nextToken } } : undefined,
      Limit: 10,
    }

    const result = await this.dynamoDbClient.send(new ScanCommand(params))

    const people = []

    if (result.Items) {
      for (const item of result.Items) {
        const person = this.parseAndCheckPerson(item)
        if (person) {
          people.push(person)
        }
      }
    }

    return { people, nextToken: result.LastEvaluatedKey?.id.S }
  }
}

class PersonWriteController extends PersonControllerBase {
  constructor(private snsClient: SNSClient, private dynamoDbClient: DynamoDBClient) {
    super()
  }

  addPerson = async (json: string): Promise<CreatePersonResponse> => {
    const person = validatePersonRequestStructure(JSON.parse(json))

    const personId = Buffer.from(person.phone).toString('base64')

    const foundPerson = await this.getPerson(personId)
    if (foundPerson) {
      throw new Error(`Person with phone ${person.phone} already exists`)
    }

    const params: PutItemCommandInput = {
      TableName: process.env.PERSON_TABLE_NAME,
      Item: {
        id: { S: personId },
        first_name: { S: person.first_name },
        last_name: { S: person.last_name },
        phone: { S: person.phone },
        address: { S: person.address },
      },
    }

    await this.dynamoDbClient.send(new PutItemCommand(params))

    const newPerson = { id: personId, ...person }

    await this.publishPersonCreated(newPerson)

    return newPerson
  }

  private publishPersonCreated = async (person: Person): Promise<void> => {
    const params: PublishCommandInput = {
      TopicArn: process.env.PERSON_CREATED_TOPIC_ARN,
      Message: JSON.stringify(person),
    }

    await this.snsClient.send(new PublishCommand(params))

    console.log('Published person created event to SNS topic')
  }

  private getPerson = async (id: string): Promise<Person | undefined> => {
    const params: GetItemCommandInput = {
      TableName: process.env.PERSON_TABLE_NAME,
      Key: {
        id: { S: id },
      },
    }

    const result = await this.dynamoDbClient.send(new GetItemCommand(params))

    if (result.Item) {
      return this.parseAndCheckPerson(result.Item)
    }

    return undefined
  }
}

export default {
  createPersonReadController: (dynamoDbClient: DynamoDBClient): PersonReadController =>
    new PersonReadController(dynamoDbClient),

  createPersonWriteController: (snsClient: SNSClient, dynamoDbClient: DynamoDBClient): PersonWriteController =>
    new PersonWriteController(snsClient, dynamoDbClient),
}
