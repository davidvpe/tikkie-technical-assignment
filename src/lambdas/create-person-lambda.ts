import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { SNSClient } from '@aws-sdk/client-sns'
import { Context, SNSMessage, SQSBatchItemFailure, SQSBatchResponse, SQSEvent } from 'aws-lambda'
import PersonControllerFactory from '../controllers/person'

const snsClient = new SNSClient({})
const dynamoDbClient = new DynamoDBClient({})

export const handler = async (event: SQSEvent, context: Context): Promise<SQSBatchResponse> => {
  const failedItems: SQSBatchItemFailure[] = []

  for (const record of event.Records) {
    try {
      const topicEvent = JSON.parse(record.body) as SNSMessage
      const body = topicEvent.Message

      const personController = PersonControllerFactory.createPersonWriteController(snsClient, dynamoDbClient)
      const newPerson = await personController.addPerson(body)

      console.log('Created new person:', newPerson)
    } catch (err) {
      console.error('Error processing event', err)

      failedItems.push({
        itemIdentifier: record.messageId,
      })

      continue
    }
  }

  return {
    batchItemFailures: failedItems,
  }
}
