import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { SNSClient } from '@aws-sdk/client-sns'
import { APIGatewayProxyResult, APIGatewayProxyWithCognitoAuthorizerEvent, Context } from 'aws-lambda'
import PersonControllerFactory from '../controllers/person'

const snsClient = new SNSClient({})
const dynamoDbClient = new DynamoDBClient({})

export const handler = async (event: APIGatewayProxyWithCognitoAuthorizerEvent): Promise<APIGatewayProxyResult> => {
  const { body } = event
  if (!body) {
    return {
      statusCode: 400,
      body: JSON.stringify({ message: 'Missing body' }),
    }
  }
  try {
    const personController = PersonControllerFactory.createPersonWriteController(snsClient, dynamoDbClient)
    const newPerson = await personController.addPerson(body)

    return {
      statusCode: 200,
      body: JSON.stringify(newPerson),
    }
  } catch (error) {
    console.error(error)

    return {
      statusCode: 400,
      body: JSON.stringify({ message: (error as Error).message, body: JSON.parse(body) }),
    }
  }
}
