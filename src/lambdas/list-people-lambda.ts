import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { APIGatewayProxyResult, APIGatewayProxyWithCognitoAuthorizerEvent, Context } from 'aws-lambda'
import PersonControllerFactory from '../controllers/person'
const dynamoDbClient = new DynamoDBClient({})

export const handler = async (event: APIGatewayProxyWithCognitoAuthorizerEvent): Promise<APIGatewayProxyResult> => {
  const personController = PersonControllerFactory.createPersonReadController(dynamoDbClient)

  const result = await personController.listPeople(event.queryStringParameters?.nextToken)

  return {
    statusCode: 200,
    body: JSON.stringify(result),
  }
}
