import { Person, PersonRequest } from './person'

export type ListPeopleResponse = {
  people: Person[]
  nextToken?: string
}

export type CreatePersonRequest = PersonRequest
export type CreatePersonResponse = Person
