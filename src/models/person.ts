import { z } from 'zod'

const personRequestSchema = z.object({
  first_name: z.string().max(100).min(2),
  last_name: z.string().max(100).min(2),
  phone: z.string().max(15).min(5),
  address: z.string().max(100),
})

const personSchema = personRequestSchema.extend({
  id: z.string(),
})

export type PersonRequest = z.infer<typeof personRequestSchema>
export type Person = z.infer<typeof personSchema>

export const validatePersonRequestStructure = (json: Record<string, any>) => {
  const isValid = personRequestSchema.safeParse(json)
  if (isValid.success) {
    return isValid.data
  } else {
    console.error(isValid.error)
    throw new Error('Invalid person request structure')
  }
}

export const validatePersonStructure = (json: Record<string, any>) => {
  const isValid = personSchema.safeParse(json)
  if (isValid.success) {
    return isValid.data
  } else {
    return undefined
  }
}
