import PersonControllerFactory from '../../src/controllers/person'
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute'
import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { SNSClient } from '@aws-sdk/client-sns'
import Mocker from '../utils/mocker'

describe('Person Controller', () => {
  let dynamoDbClient: SubstituteOf<DynamoDBClient>
  let snsClient: SubstituteOf<SNSClient>

  beforeEach(() => {
    dynamoDbClient = Substitute.for<DynamoDBClient>()
    snsClient = Substitute.for<SNSClient>()
  })

  it('should list 10 people without page token', async () => {
    Mocker.dynamodb.scan(dynamoDbClient)

    const personController = PersonControllerFactory.createPersonReadController(dynamoDbClient)

    const result = await personController.listPeople()

    expect(result).toBeDefined()
    expect(result.people).toBeDefined()
    expect(result.people.length).toBe(10)
  })

  it('should list 10 next people with page token', async () => {
    Mocker.dynamodb.scan(dynamoDbClient)

    const personController = PersonControllerFactory.createPersonReadController(dynamoDbClient)

    const firstPage = await personController.listPeople()
    const secondPage = await personController.listPeople(firstPage.nextToken)

    expect(secondPage).toBeDefined()
    expect(secondPage.people).toBeDefined()
    expect(secondPage.people.length).toBe(10)
  })

  it('should create a person', async () => {
    const userPhone = '9876543210'

    const userToCreate = { first_name: 'John', last_name: 'Doe', phone: userPhone, address: '123 Main St' }

    Mocker.dynamodb.putItem(dynamoDbClient)
    Mocker.dynamodb.getItem(dynamoDbClient, Buffer.from('userPhone').toString('base64'))
    Mocker.sns.publishMessage(snsClient)

    const personController = PersonControllerFactory.createPersonWriteController(snsClient, dynamoDbClient)

    const result = await personController.addPerson(JSON.stringify(userToCreate))

    expect(result).toBeDefined()
    expect(result.id).toBeDefined()

    const { id, ...userCreated } = result

    expect(userCreated).toEqual(userToCreate)
  })

  it('should throw error when creating a person with invalid payload', async () => {
    const userToCreate = { first_name: 'John', last_name: 'Doe', phone: '9876543210' }

    const personController = PersonControllerFactory.createPersonWriteController(snsClient, dynamoDbClient)

    await expect(personController.addPerson(JSON.stringify(userToCreate))).rejects.toThrowError()
  })

  it('should throw error when creating a person with the same phone as other users', async () => {
    const userPhone = '1234567890'

    const userToCreate = { first_name: 'John', last_name: 'Doe', phone: userPhone, address: '123 Main St' }

    Mocker.dynamodb.getItem(dynamoDbClient, Buffer.from(userPhone).toString('base64'))

    const personController = PersonControllerFactory.createPersonWriteController(snsClient, dynamoDbClient)

    await expect(personController.addPerson(JSON.stringify(userToCreate))).rejects.toThrowError()
  })
})
