import { validatePersonRequestStructure, validatePersonStructure } from '../../src/models/person'

describe('Person model', () => {
  it('person request should validate to true and return the object', () => {
    const personRequest = {
      first_name: 'John',
      last_name: 'Doe',
      phone: '9876543210',
      address: '123 Main St',
    }

    const result = validatePersonRequestStructure(personRequest)

    expect(result).toBeTruthy()
    expect(result).toEqual(personRequest)
  })

  it('person request should validate to false and throw an error', () => {
    const personRequest = {
      first_name: 'John',
      last_name: 'Doe',
      phone: '9876543210',
    }

    expect(() => validatePersonRequestStructure(personRequest)).toThrowError()
  })

  it('person should validate to true and return the object', () => {
    const person = {
      id: '123',
      first_name: 'John',
      last_name: 'Doe',
      phone: '9876543210',
      address: '123 Main St',
    }

    const result = validatePersonStructure(person)

    expect(result).toBeTruthy()
    expect(result).toEqual(person)
  })

  it('person should validate to false and return the error', () => {
    const person = {
      first_name: 'John',
      last_name: 'Doe',
      phone: '9876543210',
    }

    const result = validatePersonStructure(person)

    expect(result).toBeFalsy()
  })
})
