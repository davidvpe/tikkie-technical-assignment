import {
  DynamoDBClient,
  ScanCommand,
  ScanCommandInput,
  ScanCommandOutput,
  GetItemCommand,
  GetItemCommandOutput,
  PutItemCommand,
} from '@aws-sdk/client-dynamodb'
import { SNSClient } from '@aws-sdk/client-sns'
import { Arg, SubstituteOf } from '@fluffy-spoon/substitute'
import people from '../fixtures/data.json'

const MAX_ITEMS = 10

const getScanOutput = (nextToken?: string): ScanCommandOutput => {
  let items = people
  if (nextToken) {
    const lastItemIndex = people.findIndex((p) => p.id === nextToken)
    items = people.slice(lastItemIndex + 1)
  } else {
    items = people.slice(0, MAX_ITEMS)
  }
  const lastEvaluatedKey = items[items.length - 1].id

  return {
    $metadata: {},
    Count: MAX_ITEMS,
    Items: items.map((p) => ({
      id: { S: p.id },
      first_name: { S: p.first_name },
      last_name: { S: p.last_name },
      phone: { S: p.phone },
      address: { S: p.address },
    })),
    LastEvaluatedKey: {
      id: { S: lastEvaluatedKey },
    },
  }
}

const getGetItemOutput = (id: string): GetItemCommandOutput => {
  const person = people.find((p) => p.id === id)
  const result = {
    $metadata: {},
    Item: person
      ? {
          id: { S: person.id },
          first_name: { S: person.first_name },
          last_name: { S: person.last_name },
          phone: { S: person.phone },
          address: { S: person.address },
        }
      : undefined,
  }

  return result
}

const mockDynamoDBScan = (mockedDynamoDbClient: SubstituteOf<DynamoDBClient>) => {
  const lastKey = people[MAX_ITEMS - 1].id

  mockedDynamoDbClient
    .send(Arg.is((x) => x instanceof ScanCommand && !x.input.ExclusiveStartKey))
    .resolves(getScanOutput())
  mockedDynamoDbClient
    .send(Arg.is((x) => x instanceof ScanCommand && x.input.ExclusiveStartKey?.id.S == lastKey))
    .resolves(getScanOutput(lastKey))
}

const mockDynamoDBGetItem = (mockedDynamoDbClient: SubstituteOf<DynamoDBClient>, withId: string) => {
  mockedDynamoDbClient.send(Arg.is((x) => x instanceof GetItemCommand)).resolves(getGetItemOutput(withId))
}

const mockDynamoDBPutItem = (mockedDynamoDbClient: SubstituteOf<DynamoDBClient>) => {
  mockedDynamoDbClient.send(Arg.is((x) => x instanceof PutItemCommand)).resolves({ $metadata: {} })
}

const mockSNSPublishMessage = (mockedSNSClient: SubstituteOf<SNSClient>) => {
  mockedSNSClient.send(Arg.any()).resolves({ $metadata: {} })
}

export default {
  dynamodb: {
    scan: mockDynamoDBScan,
    putItem: mockDynamoDBPutItem,
    getItem: mockDynamoDBGetItem,
  },
  sns: {
    publishMessage: mockSNSPublishMessage,
  },
}
